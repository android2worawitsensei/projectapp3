package com.thanawat.app_3.model

data class Oil(val name: String, val price: Double)
